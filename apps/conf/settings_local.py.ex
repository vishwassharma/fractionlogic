# --------------Production env
PIPELINE = True


DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': '',                      # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD' : '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

# Set up configuration for send and receive email
# EMAIL_HOST='smtp.servername.com'
# EMAIL_HOST_USER='mailbox_username'
# EMAIL_HOST_PASSWORD='mailbox_password'
# DEFAULT_FROM_EMAIL='valid_email_address'
# SERVER_EMAIL='valid_email_address'

# Make this unique, and don't share it with anybody.
SECRET_KEY = '=d56x4adslkfalsjfdasdu83209842094px-*s@%)9)g$ck$4lbf4'


