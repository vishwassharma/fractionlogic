# Boilerplate for projects hosted on webfraction

#### Create django project using 
    http://docs.webfaction.com/software/django/getting-started.html
    > Create a aplication called logicstick from the Application Create menu [logicstick]
    > Create a static servering application > under Static > with no .htaccess > expires max [logicstatic]
    > Create a database > PostgreSql [vishwas_logicdata]
    >> Create a website with .... www.<websitename>.com and <websitename>.com to logicstick application
    >> Create a website with static.<websitename>.com to go to logicstatic application

###Change the name of the application 
    > rm -rf myproject
    >> Then either upload your project directory to this folder or else
    >> use `./bin/django-admin startproject apps` 
    > $HOME/webapps/django_app/apache2/conf/httpd.conf
    >> Find and replace all the occurance of myproject in this file with apps


#### Setting up of virtualenv
    > easy_install virtualenv
    > easy_install pip
    > pip install virtualenvwrapper
    > vim ~/.bashrc
    > > export WORKON_HOME=$HOME/.virtualenvs
    > > source /home/<username>/bin/virtualenvwrapper.sh
    > > export PIP_VIRTUALENV_BASE=$WORKON_HOME # Tell pip to create its virtualenvs in $WORKON_HOME.
    > > export PIP_RESPECT_VIRTUALENV=true # Tell pip to automatically use the currently active virtualenv
    > source ~/.bashrc
    > mkvirtualenv --no-site-packages --distribute --python python2.7 <projectname>

#### After all this edit wsgi.py file
    ```python
    import os, sys, site
    
    # Tell wsgi to add the Python site-packages to its path. 
    site.addsitedir('/home/vishwas/.virtualenvs/logic/lib/python2.7/site-packages')

    os.environ['DJANGO_SETTINGS_MODULE'] = 'apps.settings'

    activate_this = os.path.expanduser("~/.virtualenvs/logic/bin/activate_this.py")
    execfile(activate_this, dict(__file__=activate_this))
    
    # Calculate the path based on the location of the WSGI script
    project = '/home/vishwas/webapps/logicstick/apps/'
    workspace = os.path.dirname(project)
    sys.path.append(workspace)

    from django.core.handlers.wsgi import WSGIHandler
    application = WSGIHandler()```


####Set up database
    Change settings file to edit the database
    Once data base have been added
#### Set up static file server
    > STATIC_URL = 'http://static.logicstick.com'
    > STATICFILES_DIRS = '/path/to/static/apps' 

#### Configure email
    > See conf.settings.py for more details

#### configure admin site
    > This can be configured using urls in apps.urls and enabling the installed application for the same

#### Installing python modules which are required
    > find the location of pgsql using whereis pgsql
    > `export PATH=/path/to/compiled/pgsql/bin:$PATH'
    > pip install -r requirements.txt
    > This will Installing
    > > psycopg2
    > > django-pipefiles
    > > manifesto
    > > bencode

#### Install YUI compressor
    > cd <path/to/project/bin>
    > wget http://yui.zenfs.com/releases/yuicompressor/yuicompressor-2.4.7.zip
    > unzip yuicompressor-2.4.7.zip
    > ln -s yuicompressor-2.4.6 yuicompressor
    >> cd yuicompressor/build
    >> ln -s yuicompressor-2.4.7.jar yuicompressor.jar
    > Create a sh file named yuicompressor like this
    > > > #!/bin/sh
    > > > YUI_JAR=/usr/share/yuicompressor/yuicompressor.jar
    > > > java -jar $YUI_JAR "$*"
    then >> chmod a+x yuicompressor

#### Run python manager.py syncdb
    Sync database

#### Restart Apache
    <go/to/your/app>apache2/bin
    then
    `./restart`


#### Create folders for assets
    On dev server :
        
    On production server:



#### TODO : Setup ngnix
#### TODO : 


